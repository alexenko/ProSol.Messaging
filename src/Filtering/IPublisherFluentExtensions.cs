using ProSol.Messaging.Translating;

namespace ProSol.Messaging.Filtering;

public static class IPublisherFluentExtensions
{
    /// <summary>
    /// Filters a messages from a publisher.
    /// </summary>
    public static IPublisher<TMessage> Filter<TMessage>(
        this IPublisher<TMessage> publisher,
        Predicate<TMessage> filterCondition)
    {
        var retranslator = new Retranslator<TMessage>();
        var filter = new FilterSubscription<TMessage>(retranslator, filterCondition);
        var unsubscriber = publisher.Subscribe(filter);
        return retranslator;
    }

    /// <summary>
    /// Filters a messages from a publisher.
    /// Interrupts a pipeline of a publisher, when the <see cref="filterCondition"/> is met.
    /// </summary>
    public static IPublisher<TMessage> Endpoint<TMessage>(
        this IPublisher<TMessage> publisher,
        Predicate<TMessage> filterCondition)
    {
        var retranslator = new Retranslator<TMessage>();
        var filter = new EndpointSubscription<TMessage>(retranslator, filterCondition);
        var unsubscriber = publisher.Subscribe(filter);
        return retranslator;
    }
}