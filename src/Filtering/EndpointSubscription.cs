namespace ProSol.Messaging.Filtering;

internal class EndpointSubscription<TMessage>(
    IPipelineSubscriber<TMessage> subscriber,
    Predicate<TMessage> condition)
    : IPipelineSubscriber<TMessage>
{
    public void OnCompleted()
    {
    }

    public void OnNext(TMessage message, NextDelegate next)
    {
        static void dropOff() { };
        if (condition(message))
        {
            subscriber.OnNext(message, dropOff);
        }
        else
        {
            next();
        }
    }
}