namespace ProSol.Messaging.Filtering;

internal class FilterSubscription<TMessage>(
    IPipelineSubscriber<TMessage> subscriber,
    Predicate<TMessage> condition)
    : IPipelineSubscriber<TMessage>
{
    public void OnCompleted()
    {
    }

    public void OnNext(TMessage message, NextDelegate next)
    {
        if (condition(message))
        {
            subscriber.OnNext(message, next);
        }
        else
        {
            next();
        }
    }
}