namespace ProSol.Messaging.Translating;

/// <summary>
/// A dummy subscriber which just pushes a message.
/// Usefull for custom wrappers around a subscribers.
/// </summary>
public sealed class Retranslator<TMessage> : TranslatorBase<TMessage, TMessage>
{
    protected override TMessage ConvertMessage(TMessage message) => message;
}