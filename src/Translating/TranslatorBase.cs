namespace ProSol.Messaging.Translating;

public abstract class TranslatorBase<TSource, TDest>
    : IPipelineSubscriber<TSource>, IPublisher<TDest>, IDisposable
{
    private IPipelineSubscriber<TDest>? destSubscriber;

    /// <inheritdoc/>
    public void OnCompleted() => destSubscriber?.OnCompleted();

    /// <inheritdoc/>
    public void OnNext(TSource message, NextDelegate next) 
    {
        var dest = ConvertMessage(message);
        if (destSubscriber != null)
        {
            this.Publish(dest, destSubscriber, next);
        }
    }

    /// <summary>
    /// Converts a message from one type to another.
    /// </summary>
    /// <param name="message"> Source message. </param>
    /// <returns> Destination message or null if unconvertable. </returns>
    /// <remarks>
    /// - The NextDelegate is triggered by target subsriber, or by this class if unconvertable.
    /// </remarks>
    protected abstract TDest ConvertMessage(TSource message);

    public void Publish(TDest message)
    {
        if (destSubscriber != null)
        {
            this.Publish(message, destSubscriber, () => {});
        }
    }

    public IDisposable Subscribe(IPipelineSubscriber<TDest> subscriber)
    {
        if (destSubscriber != null)
        {
            throw new InvalidOperationException("Unable to subscribe more than one subscriber.");
        }
        destSubscriber = subscriber;
        return this;
    }

    public void Dispose()
    {
        destSubscriber?.OnCompleted();
        destSubscriber = null;
    }
}
