namespace ProSol.Messaging.Translating;

public static class IPublisherFluentExtensions
{
    public static IPublisher<TDest> Translate<TSource, TDest>(
        this IPublisher<TSource> publisher,
        Func<TSource, TDest> converter) 
        => Translate(publisher, new SimpleTranslator<TSource, TDest>(converter));

    public static IPublisher<TDest> Translate<TSource, TDest>(
        this IPublisher<TSource> publisher,
        TranslatorBase<TSource, TDest> translatorBase)
    {
        var unsubscriber = publisher.Subscribe(translatorBase);
        return translatorBase;
    }
}
