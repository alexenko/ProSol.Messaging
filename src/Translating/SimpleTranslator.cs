namespace ProSol.Messaging.Translating;

public sealed class SimpleTranslator<TSource, TDest>(Func<TSource, TDest> converter) 
    : TranslatorBase<TSource, TDest>
{
    protected override TDest ConvertMessage(TSource message)
        => converter(message);
}