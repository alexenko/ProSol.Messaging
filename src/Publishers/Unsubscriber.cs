﻿namespace ProSol.Messaging;

public sealed class Unsubscriber<TMessage> : IDisposable
{
    private readonly ICollection<IPipelineSubscriber<TMessage>> subscribers;
    private readonly IPipelineSubscriber<TMessage> target;

    internal Unsubscriber(
        ICollection<IPipelineSubscriber<TMessage>> subscribers, 
        IPipelineSubscriber<TMessage> target)
    {
        this.subscribers = subscribers;
        this.target = target;
    }

    public void Dispose()
    {
        subscribers.Remove(target);
    }
}