using ProSol.Messaging.Translating;

namespace ProSol.Messaging.Acting;

internal class ActionSubscription<TMessage>(Action<TMessage> action)
    : TranslatorBase<TMessage, TMessage>
{
    protected override TMessage ConvertMessage(TMessage message)
    {
        action(message);
        return message;
    }
}