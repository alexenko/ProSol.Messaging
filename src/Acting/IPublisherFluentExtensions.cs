namespace ProSol.Messaging.Acting;

public static class IPublisherFluentExtensions
{
    /// <summary>
    /// Filters a messages from a publisher.
    /// </summary>
    public static IPublisher<TMessage> Act<TMessage>(
        this IPublisher<TMessage> publisher,
        Action<TMessage> action)
    {
        var subscription = new ActionSubscription<TMessage>(action);
        var unsubscriber = publisher.Subscribe(subscription);
        return subscription;
    }

    
}