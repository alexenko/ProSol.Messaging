namespace ProSol.Messaging;

/// <summary>
/// A Publisher/Observable object.
/// </summary>
public interface IPublisher<TMessage>
{
    void Publish(TMessage message);

    /// <summary>
    /// Subscribe a subscriber.
    /// </summary>
    /// <param name="subscriber"> Target. </param>
    /// <returns> Custom IDisposable implementation. </returns>
    IDisposable Subscribe(IPipelineSubscriber<TMessage> subscriber);
}
