namespace ProSol.Messaging;

public static class PublishHelper
{
    public static void Publish<TMessage>(
        this IPublisher<TMessage> publisher, 
        TMessage message, 
        params IPipelineSubscriber<TMessage>[] subscribers)
    {
        if (subscribers.Length == 0)
        {
            return;
        }
        
        void next() => publisher.Publish(message, subscribers[1..]);
        publisher.Publish(message, subscribers[0], next);
    }

    public static void Publish<TMessage>(
        this IPublisher<TMessage> publisher, 
        TMessage message, 
        IPipelineSubscriber<TMessage> subscriber, 
        NextDelegate next)
    {
        switch (subscriber)
        {
            case null: return;
            case IPipelineSubscriber<TMessage> pipelineSubscriber:
            {
                pipelineSubscriber.OnNext(message, next);
                return;
            }
            default: throw new ArgumentException($"Incorrect subscriber: {subscriber}");
        }
    }
}