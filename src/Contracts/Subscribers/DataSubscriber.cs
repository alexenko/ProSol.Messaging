using System.Collections.Immutable;

namespace ProSol.Messaging;

public sealed class DataSubscriber<TMessage> : IPipelineSubscriber<TMessage>
{
    private readonly List<TMessage> messages = [];

    public ImmutableArray<TMessage> Messages => [..messages];

    public void OnCompleted()
    {
    }

    public void OnNext(TMessage message, NextDelegate next)
    {
        messages.Add(message);
        next();
    }
}