namespace ProSol.Messaging;

public delegate void NextDelegate();

/// <summary>
/// An Observer/Subscriber object with pipeline support.
/// </summary>
public interface IPipelineSubscriber<TMessage>
{
    /// <summary>
    /// Represents a reaction on a message.
    /// </summary>
    /// <param name="message"> A message. </param>
    /// <param name="next"> A delegate to proceed to next <see cref="ISubscriber"/>, like ASP.NET pipeline. </param>
    /// <remarks>
    /// - For more details about ASP.NET pipeline, see https://learn.microsoft.com/en-us/aspnet/core/fundamentals/middleware/?view=aspnetcore-8.0
    /// </remarks>
    void OnNext(TMessage message, NextDelegate next);

    /// <summary>
    /// Finalizes a subscriber.
    /// </summary>
    void OnCompleted();
}