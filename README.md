# ProSol.Messaging

Implements a message broker with ability to build a pipeline of listeners.

For example, let's make a pipeline which detects future and past dates:

Add package:
```sh
dotnet add package ProSol.Messaging --version 4.0
```

```csharp
using ProSol.Messaging;
using ProSol.Messaging.Filtering;
using ProSol.Messaging.Acting;

var provider = new PipelineMessagePublisher<DateTime>();

provider
    .Endpoint(x => x >= DateTime.Now)
    .Act(x => Console.WriteLine($"Future: {x}"));

provider
    .Act(x => Console.WriteLine($"Past: {x}"));

provider.Publish(DateTime.Today.AddDays(1));
provider.Publish(DateTime.Today.AddDays(-1));
```

Try this and you will see two messages in the console, with dates, depending on your current time: 
```
Future: ...
Past: ...
```

That's it! It's basically a pipeline builder for dispatching the messages.

There are some more extension methods for dispatching, you may find there:
- observer-related interfaces [here](/src/Contracts/).
- `PipelineMessagePublisher` [here](/src/Publishers/PipelineMessagePublisher.cs).
- [Translating](/docs/Translating.md) for messages.
- [Filtering](/docs/Filtering.md) for messages.

Happy coding!