namespace ProSol.Messaging.Subscriptions.Tests;

public class RegularSubscriptionTests
{
    [Test]
    public void RegularSubscription_ShouldAccept_Keywords()
    {
        static bool predicate(string x) => x.Contains("ERROR") || x.Contains("CRITICAL");

        string[] source = [
            "quick brown fox",
            "introduced an ERROR",
            "so the lazy dog",
            "got a CRITICAL message"
        ];

        string[] expected = [
            "introduced an ERROR",
            "got a CRITICAL message"
        ];

        var subscriber = new DataSubscriber<string>();
        var subscription = new RegularSubscription<string>(subscriber, predicate);
        
        foreach (var item in source)
        {
            subscription.OnNext(item);
        }

        Assert.That(subscriber.Messages, Is.EquivalentTo(expected));
    }
}