namespace ProSol.Messaging.Tests.Common;

public class TestPublisher<TMessage>(IEnumerable<TMessage> messages) : IPublisher<TMessage>
{
    readonly Queue<TMessage> messages = new(messages);
    IPipelineSubscriber<TMessage>? subscriber;

    public void PublishAll()
    {
        while(messages.Count != 0)
        {
            Publish(messages.Dequeue());
        }
    }

    public void Publish(TMessage message)
    {
        this.Publish(message, subscriber!);
    }
        
    public IDisposable Subscribe(IPipelineSubscriber<TMessage> subscriber)
    {
        this.subscriber = subscriber;
        #pragma warning disable CS8603
        return null;
        #pragma warning restore CS8603
    }
}