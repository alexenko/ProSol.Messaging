namespace ProSol.Messaging.Acting.Tests;

public class ActingTests
{
    [Test]
    public void Act_ShouldWork()
    {
        var publisher = new PipelineMessagePublisher<string>();
        var result = "";
        publisher.Act(x => result = x);

        publisher.Publish("Test");

        Assert.That(result, Is.EqualTo("Test"));
    }

    [Test]
    public void Act_ShouldWork_2()
    {
        var publisher = new PipelineMessagePublisher<string>();
        var result = "";
        publisher
            .Act(x => result+= x)
            .Act(x => result+= x);

        publisher.Publish("Test");

        Assert.That(result, Is.EqualTo("TestTest"));
    }
}