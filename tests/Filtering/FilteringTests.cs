using ProSol.Messaging.Translating;

namespace ProSol.Messaging.Filtering.Tests;

public class FilteringTests
{
    [Test]
    public void Endpoint_ShouldWork()
    {
        string[] input = [
            "nothing happens",
            "WARN: warning1",
            "WARN: warning2",
            "CRIT: critical",
            "nothing happens",
        ];

        var expected = new
        {
            warn = new string[] {
                "WARN: warning1",
                "WARN: warning2",
            },
            crit = new string[] 
            {
                "CRIT: critical",
            },
            rest = new string[] {
                "nothing happens",
                "nothing happens"
            }
        };

        var publisher = new PipelineMessagePublisher<string>();
        var warnCollector = new DataSubscriber<string>();
        var critCollector = new DataSubscriber<string>();
        var restCollector = new DataSubscriber<string>();

        publisher
            .Endpoint<string>(x => x.Contains("WARN:"))
            .Subscribe(warnCollector);

        publisher
            .Endpoint<string>(x => x.Contains("CRIT:"))
            .Subscribe(critCollector);

        publisher
            .Subscribe(restCollector);

        foreach (var message in input)
        {
            publisher.Publish(message);
        }

        Assert.Multiple(() =>
        {
            Assert.That(warnCollector.Messages, Is.EquivalentTo(expected.warn));
            Assert.That(critCollector.Messages, Is.EquivalentTo(expected.crit));
            Assert.That(restCollector.Messages, Is.EquivalentTo(expected.rest));
        });
    }

    [Test]
    public void Filter_ShouldWork()
    {
        string[] input = [
            "nothing happens",
            "WARN: warning1",
            "WARN: warning2",
            "CRIT: critical",
            "nothing happens",
        ];

        var publisher = new PipelineMessagePublisher<string>();
        var warnsCountCollector = new DataSubscriber<bool>();
        var critsCountCollector = new DataSubscriber<bool>();
        var restCollector = new DataSubscriber<string>();

        publisher
            .Filter<string>(x => x.Contains("WARN:"))
            .Translate<string, bool>(x => true)
            .Subscribe(warnsCountCollector);

        publisher
            .Filter<string>(x => x.Contains("CRIT:"))
            .Translate<string, bool>(x => true)
            .Subscribe(critsCountCollector);

        publisher
            .Subscribe(restCollector);

        foreach (var message in input)
        {
            publisher.Publish(message);
        }

        Assert.Multiple(() =>
        {
            Assert.That(warnsCountCollector.Messages, Has.Length.EqualTo(2));
            Assert.That(critsCountCollector.Messages, Has.Length.EqualTo(1));
            Assert.That(restCollector.Messages, Is.EquivalentTo(input));
        });
    }
}