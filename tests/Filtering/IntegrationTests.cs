namespace Filtering.Tests;

public class IntegrationTests
{
    [Test]
    public void Check_Pipeline_Chain_Works()
    {
        var publisher = new PipelineMessagePublisher<string>();
        var data = new DataSubscriber<string>();

        publisher
            .Filter<string>(x => x.StartsWith("CRIT"))
            .Subscribe(data);

        publisher.Publish("CRITICAL message");
        publisher.Publish("Verbose message");

        Assert.That(data.Messages, Has.Length.EqualTo(1));
        Assert.That(data.Messages.First(), Is.EqualTo("CRITICAL message"));
    }
}