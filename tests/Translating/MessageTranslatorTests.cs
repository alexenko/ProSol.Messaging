using ProSol.Messaging.Tests.Common;

namespace ProSol.Messaging.Translating.Tests;

public class MessageTranslatorTests
{
    [Test]
    public void Translating_MultipleChains_ShouldWork()
    {
        // Arrange configs.
        string[] input = ["Hello", "World!"];
        bool[] expected = [false, true];

        // Arrange operations.
        var dataSubscriber = new DataSubscriber<bool>();
        var publisher = new TestPublisher<string>(input);
        using var unsub = publisher
            .Translate<string, int>(x => x.Length)
            .Translate<int, bool>(x => x > 5)
            .Subscribe(dataSubscriber);

        // Act.
        publisher.PublishAll();

        // Assert.
        Assert.That(dataSubscriber.Messages, Is.EquivalentTo(expected));
    }
}