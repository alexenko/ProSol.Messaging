using ProSol.Messaging;
using ProSol.Messaging.Translating;
using ProSol.Messaging.Filtering;

namespace Translating.Tests;

public class IntegrationTests
{
    [Test]
    public void Check_Pipeline_Chain_Works()
    {
        var publisher = new PipelineMessagePublisher<string>();
        var data = new DataSubscriber<int>();

        publisher
            .Translate<string, int>(x => x.Length)
            .Subscribe(data);

        publisher.Publish("FOO");

        Assert.That(data.Messages, Has.Length.EqualTo(1));
        Assert.That(data.Messages.First(), Is.EqualTo(3));
    }

    [Test]
    public void Check_Pipeline_Fallback_Chain_WithTranslators_Works()
    {
        var publisher = new PipelineMessagePublisher<int>();
        var crits = new DataSubscriber<string>();
        var notCrits = new DataSubscriber<string>();

        publisher
            .Translate<int, string>(x => x > 10 ? "CRITICAL message" : "")
            .Endpoint<string>(x => x.StartsWith("CRIT"))
            .Subscribe(crits);

        publisher
            .Translate<int, string>( x => $"Verbose message {x}")
            .Subscribe(notCrits);

        publisher.Publish(9);
        publisher.Publish(11);

        Assert.Multiple(() =>
        {
            Assert.That(crits.Messages, Has.Length.EqualTo(1));
            Assert.That(crits.Messages.First(), Is.EqualTo("CRITICAL message"));

            Assert.That(notCrits.Messages, Has.Length.EqualTo(1));
            Assert.That(notCrits.Messages.First(), Is.EqualTo("Verbose message 9"));
        });
    }
}