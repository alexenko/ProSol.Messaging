# ProSol.Messaging.Filtering

Allows to catch a specific message by using a predicate.
Either clones a message to a subscriber behind via ('.Filter()`) method,
or swallows a message out of the pipeline via ('.Endpoint()') method.

# Demo

## Filter

Let's create an app, which logs all strings provided.
But counts specific strings in separate subscribers.
So the output will demonstrate how many specific messages passed.

```
dotnet add package ProSol.Messaging --version 4.0.0-rc.8.0
```

### Input
Let the messages look like this:

```csharp
string[] input = [
    "nothing happens",
    "WARN: warning1",
    "WARN: warning2",
    "CRIT: critical",
    "nothing happens",
];
```

### Pipeline configuration
The pipeline configuration consists of three ways: warns, crits and rest collectors:

```csharp
var publisher = new PipelineMessagePublisher<string>();
var warnsCountCollector = new DataSubscriber<bool>();
var critsCountCollector = new DataSubscriber<bool>();
var restCollector = new DataSubscriber<string>();
```

Lets add three subscriber to the publisher:
1. Reacts on any "WARN:" messages.
2. Reacts on any "CRIT:" messages.
3. Gets all messages.

```csharp
publisher
    .Filter<string>(x => x.Contains("WARN:"))
    .Translate<string, bool>(x => true)
    .Subscribe(warnsCountCollector);

publisher
    .Filter<string>(x => x.Contains("CRIT:"))
    .Translate<string, bool>(x => true)
    .Subscribe(critsCountCollector);

publisher
    .Subscribe(restCollector);
```

### Try

Lets try this configuration against provided [input](#input):

```csharp
foreach (var message in input)
{
    publisher.Publish(message);
}
```

The output is:
```
warnsCountCollector.Messages.Count() == 2;
critsCountCollector.Messages.Count() == 1;
restCollector.Messages; // Contains all messages from input.
```

So, the publisher got three subscribers, every message reaches on every subscriber, but two of subscribers are limited by Filter so they react only on specific messages.

But what if there is a need to swallow a message when the filter condition is met?
Let's see the [Endpoints](#endpoint) for that.

## Endpoint

Let's just change Filter for Endpoint methods:
```csharp
publisher
    .Endpoint<string>(x => x.Contains("WARN:"))
    .Translate<string, bool>(x => true)
    .Subscribe(warnsCountCollector);

publisher
    .Endpoint<string>(x => x.Contains("CRIT:"))
    .Translate<string, bool>(x => true)
    .Subscribe(critsCountCollector);

publisher
    .Subscribe(restCollector);
```

Then run them with same [input](#input):

```csharp
foreach (var message in input)
{
    publisher.Publish(message);
}
```

And see the output:
```csharp
warnsCountCollector.Messages.Count() == 2;
critsCountCollector.Messages.Count() == 1;

restCollector.Messages; 
// OUTPUT:
// nothing happens
// nothing happens
```

So, the `restCollector` only getting the **unprocessed* messages, and acts like a fallback subscriber in this case. 

# Conclusion

So, basically, you can either react or catch a message with `Filter` or `Endpoint` methods respectively.

- [BACK](../README.md)