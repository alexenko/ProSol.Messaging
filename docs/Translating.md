# ProSol.Messaging.Translating

Allows to translate one message type to another with one line via `.Translate` method.

## Demo

For example, lets take a string tokens, take their length, and see if it's longer that some threshold.

Add the package:
```
dotnet add package ProSol.Messaging --version 4.0.0-rc.5.0
```

```csharp
// Include the package
using ProSol.Messaging;
using ProSol.Messaging.Translating;

// Declare some starting and finishing point:
var dataSubscriber = new DataSubscriber<bool>(); // a dummy subscriber which just collects data in List.
var publisher = new SomePublisher<string>(); // a custom dummy publisher, see below.

// Declare a pipeline:
publisher
    .Translate<string, int>(x => x.Length)
    .Translate<int, bool>(x => x > 5)
    .Subscribe(dataSubscriber);


// Process some messages:
string[] input = ["Hello", "World!"];
foreach (var item in source)
{
    publisher.Publish(item);
}

// Render the ouput:
Console.WriteLine(dataSubscriber.Messages);

// OUTPUT:
// false
// true
```

A dummy publisher looks like this:
```csharp
public class SomePublisher<TData> : IPublisher<TData>
{
    private ISubscriber? subscriber;

    public void Publish(TData message)
    {
        PublishHelper.Publish(message, subscriber!);
    }

    public IDisposable Subscribe(ISubscriber subscriber)
    {
        this.subscriber = subscriber;
        return null;
    }
}
```

So, there is no need to declare one-line subscribers just to convert one message to another.
The `dataSubscriber` is, basically, subscriber onto the `publisher` messages, with some middleware adapters on the way.

Happy coding!

- [BACK](../README.md)